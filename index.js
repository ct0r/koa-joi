const originalJoi = require('joi')

function koaJoi (joi = originalJoi) {
  return (ctx, schema) => {
    const input = {}

    for (const key in schema) {
      input[key] = ctx.request[key] === undefined ? ctx[key] : ctx.request[key]
    }

    const { value, error } = joi.validate(input, schema)

    if (error) throw error
    return value
  }
}

koaJoi.validate = koaJoi()

module.exports = koaJoi
