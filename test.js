const joi = require('joi')
const test = require('ava')
const sinon = require('sinon')
const koaJoi = require('.')

const schema = {
  params: {
    id: joi.number().required()
  },
  body: {
    name: joi.string().max(8).trim().required()
  }
}

test('validate with valid input returns sanitized data', t => {
  const ctx = {
    method: 'POST',
    params: {
      id: '1'
    },
    request: {
      body: {
        name: ' Jon Snow '
      }
    }
  }

  const result = koaJoi.validate(ctx, schema)

  t.deepEqual(result, {
    params: {
      id: 1
    },
    body: {
      name: 'Jon Snow'
    }
  })
})

test('validate with invalid input throws joi error', t => {
  const ctx = {
    request: {
      body: {}
    }
  }

  const error = t.throws(_ => koaJoi.validate(ctx, schema))

  t.true(error.isJoi)
})

test('validate uses provided joi instance', t => {
  const ctx = {
    params: {},
    request: {}
  }
  const value = {}
  const validateSpy = sinon.spy(_ => ({ value }))
  const myJoi = {
    validate: validateSpy
  }

  const result = koaJoi(myJoi)(ctx, schema)

  t.true(validateSpy.calledOnce)
  t.deepEqual(result, value)
})
